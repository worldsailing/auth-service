<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

$console = new Application('Sample command', 'n/a');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console
    ->register('sample')
    ->setDefinition(array(
        // Put some variable here
         new InputOption('some-option', null, InputOption::VALUE_OPTIONAL, 'Some help'),

    ))
    ->setDescription('Sample command description')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        // do something
        require_once PATH_SRC . '/command/Sample.php';
        Sample::doSomething($input);
    })
;

return $console;
