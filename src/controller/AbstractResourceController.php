<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

/**
 * Class AbstractResourceController
 * @package Controller
 */
Abstract class AbstractResourceController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     */
    public function verifyToken(Application $app)
    {
        if (! $app['debug.visual']) {
            if (!$app['service.oauth.server']->verifyResourceRequest(\OAuth2\Request::createFromGlobals())) {
                $app['service.oauth.server']->getResponse()->send();
                die();
            }
        }
    }
}
