<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

/**
 * Class TokenController
 * @package Controller
 */
class TokenController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        parent::connect($app);

        $controller = $app['controllers_factory'];
        /*
         * Register available methods
         */
        $controller->post("/", array( $this, 'index' ) )->bind( 'token_request' );


        return $controller;
    }

    /**
     * Basic token request
     * @param Application $app
     */
    public function index(Application $app )
    {
        $app['service.oauth.server']->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();
        die();
    }

}
