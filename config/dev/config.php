<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

require PATH_ROOT . '/config/config.php';
//DB parameters
$app['dbs.config'] = [
    /*
    'isaf' => [
        'driver' => 'pdo_mysql',
        'charset'  => 'utf8',
        'host'     => '127.0.0.1',
        'dbname'   => 'isaf',
        'user'     => 'root',
        'password' => 'root'
    ]*/
];

//OAuth parameters
$app['oauth.config'] = [
    'storage' => 'pdo',
    'connection' => [
        'dsn' => 'mysql:dbname=oauth;host=localhost',
        'user' => 'root',
        'password' => 'root'
    ]
];

// enable the debug mode
$app['debug'] = false;
$app['debug.visual'] = false; //response is html to screen. It's only for local dev area.
